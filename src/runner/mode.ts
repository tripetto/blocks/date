export type TMode =
    | "equal"
    | "not-equal"
    | "before"
    | "after"
    | "between"
    | "not-between"
    | "defined"
    | "undefined";
