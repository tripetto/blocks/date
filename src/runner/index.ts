/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    markdownifyToString,
    validator,
} from "@tripetto/runner";
import "./condition";

export interface IDateTime {
    readonly time?: boolean;
    readonly range?: boolean;
    readonly placeholder?: string;
}

export abstract class DateTime extends NodeBlock<IDateTime> {
    readonly dateSlot = assert(this.valueOf<number, Slots.Date>("date"));
    readonly toSlot = this.valueOf<number, Slots.Date>("to");
    readonly required = this.dateSlot.slot.required || false;

    get fromError(): boolean {
        return !this.dateSlot.hasValue || false;
    }

    get toError(): boolean {
        return (
            !this.toSlot ||
            !this.toSlot.hasValue ||
            this.toSlot.value < this.dateSlot.value
        );
    }

    get toPlaceholder(): string {
        return (
            markdownifyToString(this.props.placeholder || "", this.context) ||
            ""
        );
    }

    @validator
    validate(): boolean {
        if (this.props.range) {
            return (
                (this.toSlot &&
                    this.dateSlot.hasValue === this.toSlot.hasValue &&
                    this.toSlot.value >= this.dateSlot.value) ||
                false
            );
        }

        return true;
    }
}
